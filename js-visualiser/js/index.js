"use strict";
/**
 * Visualiser Class. Creates Visualizer.
 * @param canvas HTML5 Canvas Element
 */
class Visualizer {
  constructor(canvas) {
    this.canvas = canvas; /** @type {} */
    this.ctx = new AudioContext(); /**  @type {AudioContext} */
    this.analyser = this.ctx.createAnalyser(); /** @type {Au} */
    this.isJumpy = false;
    this.isCurved = true;
    this.fftSize = 4096;
    this.font = "1.5em Arial";
  }

  /**
   * Connect Audio Element or Audio Stream
   * @param {string} str The String "Stream" or "Audio"
   * @param {Audio|MediaStream} src HTML5 Audio Element or Stream
   */
  connect(str, src) {
    let source;
    if (str === "Audio") source = this.ctx.createMediaElementSource(src);
    else if (str === "Stream") source = this.ctx.createMediaStreamSource(src);
    else throw MediaError("src is not a HTML5Audio or MediaStream");

    source.connect(this.analyser);
    this.analyser.connect(this.ctx.destination);
  }

  handleCanvas() { //TODO: Remove
   document.addEventListener('keydown', e => {
     if (e.keyCode === 67) { //c was pressed
        this.isCurved = !this.isCurved;
        if (this.isJumpy) this.isCurved = false;
     } else if (e.keyCode === 74) {
       this.isJumpy = !this.isJumpy
       if (this.isCurved) this.isJumpy = false;
     }
   })
  }

  resizeCanvas() {
    const width = this.canvas.clientWidth;
    const height = this.canvas.clientHeight;

    if (this.canvas.width !== width || this.canvas.height !== height) {
      this.canvas.width = width;
      this.canvas.height = height;
    }
  }

  visualize(barNum) {
    this.handleCanvas();
    this.resizeCanvas();
    const canvas2D = this.canvas.getContext("2d");
    this.analyser.fftSize = this.fftSize;
    const bars = barNum;
    const step = 2;
    const buffLength = this.analyser.frequencyBinCount;
    const data = new Uint8Array(buffLength);
    const barWidth = ~~(this.canvas.width / bars);
    canvas2D.font = this.font;
    let dpb = ~~(data.length / bars); /** Data Per Bar */
    if (dpb < 1) dpb = 1;

    const draw = () => {
      const t1 = performance.now();
      canvas2D.clearRect(0, 0, this.canvas.width, this.canvas.height);
      const visual = requestAnimationFrame(draw);

      this.analyser.getByteFrequencyData(data);
      canvas2D.fillStyle = "#FFFFFF";
      canvas2D.fillRect(0, 0, this.canvas.width, this.canvas.height);

      let barHeight,
        x = 0,
        curve = 1 / 2,
        index = 0,
        totalHeight = 0,
        angle = 0;

      for (let i = 0; i < bars; i++) {
        let res = this.average(data, index, dpb);
        index = res[1];

        barHeight = res[0];
        totalHeight += barHeight;

        if (this.isCurved) {
          barHeight /= curve;
         // curve += 1 / 128;
        }

        if (angle > 360) angle = 0;
        canvas2D.fillStyle = `hsl(${angle}, 100%, 50%)`;
        angle += ~~(angle / bars) || 1;

        if (this.isJumpy) barHeight * 2;

        canvas2D.fillRect(
          x,
          this.canvas.height - barHeight,
          barWidth,
          barHeight
        );
        x += barWidth + step;
      }

      canvas2D.fillStyle = "#000000";
      let ms = (performance.now() - t1).toFixed(2);
      canvas2D.fillText(
        `Frametime: ${ms}ms | Average Bar Height: ${(
          totalHeight / bars
        ).toFixed(2)}`,
        30,
        50
      );
    };
    draw();
  }

  average(data, index, dpb) {
    const start = index;
    let total = 0;

    while (index < start + dpb) {
      total += data[index];
      index++;
    }

    return [total / dpb, index];
  }
}

const visualizer = new Visualizer(document.getElementById("visualizer"));

navigator.mediaDevices.getUserMedia({audio: true}).then(stream => {
  visualizer.connect("Stream", stream);
  visualizer.visualize(200);
});


document.addEventListener('dblclick', e => {
  e.preventDefault();
  let input = document.createElement('input');
  input.type = 'file';

  let ev = document.createEvent('MouseEvents');
  ev.initEvent('click', true, false);
  input.dispatchEvent(ev);

  input.addEventListener('change', e => {
    let file = input.files[0];

    if (file.type.match(/audio/gi)) {
      console.log("File Accepted");
      let audio = new Audio();
      audio.src = window.URL.createObjectURL(file);
      visualizer.connect("Audio", audio);
      visualizer.visualize(200);
      audio.play();
    } else throw new Error('File Provided was not an audio file.');
  });
});