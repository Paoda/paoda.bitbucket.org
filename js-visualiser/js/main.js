"use strict";

const audio = new Audio();
const audioCtx = new AudioContext();
const analyser = audioCtx.createAnalyser();

window.addEventListener("load", () => {
  const source = audioCtx.createMediaElementSource(audio);
  source.connect(analyser);
  analyser.connect(audioCtx.destination);
  visualize();
});

window.addEventListener("resize", resize, false);

const canvas = document.createElement("canvas");
canvas.id = "visualiser";
canvas.draggable = true;
document.body.appendChild(canvas);
resize();

function resize() {
  const width = canvas.clientWidth;
  const height = canvas.clientHeight;

  if (canvas.width !== width || canvas.height !== height) {
    canvas.width = width;
    canvas.height = height;
  }
}

//Canvas Events START
document.onkeydown = (e) => {
  console.log(e);
  if (e.keyCode === 67) isCurve = !isCurve;

  if (isCurve) isJumpy = false;
};

canvas.onclick = e => {
  isJumpy = !isJumpy;

  if (isCurve) isJumpy = false;
}

canvas.ondragover = e => e.preventDefault();

canvas.ondrop = e => {
  e.preventDefault();

  let dt = e.dataTransfer;

  if (dt.items) {
    for (let i = 0; i < dt.items.length; i++) {
      if (dt.items[i].kind === "file") {
        let file = dt.items[i].getAsFile();

        if (file.type.match(/audio/gi)) {
          audio.src = window.URL.createObjectURL(file);
          audio.play();
        } else throw TypeError("File Uploaded was not an Audio File");
      } else throw TypeError("File Uploaded was not an Audio File");
    }
  } else {
    if (dt.files.length > 1) {
      console.log("Not a Item");
      let file = dt.files[0];

      if (file.type.match(/audio/gi)) {
        audio.src = window.URL.createObjectURL(file);
        audio.play();
      } else throw TypeError("File Uploaded was not an Audio File");
    }
  }
};

canvas.ondragend = e => {
  let dt = e.dataTransfer;
  if (dt.items) {
    for (let i = 0; i < dt.items.length; i++) {
      dt.items.remove(i);
    }
  } else {
    dt.clearData();
  }
};

canvas.ondblclick = e => {
  e.preventDefault();
  let input = document.createElement("input");
  input.type = "file";

  let event = document.createEvent("MouseEvents");
  event.initEvent("click", true, false);
  input.dispatchEvent(event);

  input.onchange = e => {
    let file = input.files[0];

    if (file.type.match(/audio/gi)) {
      audio.src = window.URL.createObjectURL(file);
      audio.play();
    } else throw TypeError("File Uploaded was not an Audio File");
  };
};

//Canvas Events END
let isCurve = true;
let isJumpy = false;
const canvasCtx = canvas.getContext("2d");

const bars = 200;
analyser.fftSize = 4096;

function visualize() {
  const bufferLength = analyser.frequencyBinCount;
  console.log(bufferLength);
  const dataArray = new Uint8Array(bufferLength);
  canvasCtx.font = "1.5em Arial";
  let dataPerBar = ~~(dataArray.length / bars);
  if (dataPerBar < 1) dataPerBar = 1;
  const barWidth = ~~(canvas.width / bars);

  function draw() {
    const t1 = performance.now();

    canvasCtx.clearRect(0, 0, canvas.width, canvas.height);

    const drawVisual = requestAnimationFrame(draw);

    analyser.getByteFrequencyData(dataArray);

    canvasCtx.fillStyle = "#F1F0FF";
    canvasCtx.fillRect(0, 0, canvas.width, canvas.height);

    let barHeight,
      x = 0,
      curve = 1/2,
      index = 0,
      totalHeight = 0;

    for (var i = 0; i < bars; i++) {
      let res = average(dataArray, index, dataPerBar);
      index = res[1];

      barHeight = res[0];
      totalHeight += res[0];
      if (isCurve) {
        barHeight /= curve;
        curve += 1/128;
      }

      canvasCtx.fillStyle = "rgb(75,0," + (barHeight + 100) + ")";

      if (isJumpy) barHeight *= 2;

      canvasCtx.fillRect(x, canvas.height - barHeight, barWidth, barHeight);
      x += barWidth + 2;
    }
    canvasCtx.fillStyle = "#000000";
    let ms = (performance.now() - t1).toFixed(2);
    canvasCtx.fillText(
      `Frametime: ${ms}ms | Average Bar Height: ${(totalHeight / bars).toFixed(2)}`,
      30,
      50
    );
  }
  draw();
}

function average(dataArr, index, dataPerBar) {
  //debugger;
  const start = index;
  let total = 0;
  while (index < start + dataPerBar) {
    total += dataArr[index];
    index++;
  }

  return [total / dataPerBar, index];
}

function median(dataArr, index, dataPerBar) {
  if (!audio.paused) debugger;
  let arr = dataArr.slice(index, index + dataPerBar);
  index += dataPerBar;

  arr = arr.sort((a, b) => a - b);
  let half = ~~(arr.length / 2);

  if (arr.length % 2) return [arr[half], index];
  return [arr[half - 1] + arr[half] / 2, index];
}

function mode(dataArr, index, dataPerBar) {
  let modes = [],
    count = [],
    number,
    maxIndex = 0;

  const start = index;

  while (index < start + dataPerBar) {
    number = dataArr[index];
    count[number] = (count[number] || 0) + 1;
    if (count[number] > maxIndex) maxIndex = count[number];
    index++;
  }

  for (let i in count)
    if (count.hasOwnProperty(i)) {
      if (count[i] === maxIndex) {
        modes.push(Number(i));
      }
    }

  let avg = modes.reduce((sum, value) => sum + value) / modes.length;
  if (!audio.paused) debugger;
  return [avg, index];
}
