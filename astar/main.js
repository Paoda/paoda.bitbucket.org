'use strict';
var Global = {};
Global.canvasWidth = 500;
Global.canvasHeight = 500;
Global.cols = 50;
Global.rows = 50;
Global.grid = new Array(Global.cols);
Global.fps = 60;


class Buttons {
    constructor() {
        this.createButtons();
    }
    createButtons() {
        let button = document.createElement('button');
        button.innerHTML = 'Run A*';
        button.id = 'astarBtn';
        button.onclick = () => {
            var astar = new Astar(document.getElementById('grid'), Global.rows, Global.cols, Global.grid, setInterval(() => {
                astar.run();
            }, 1000 / Global.fps));
        };
        document.body.appendChild(button);

        button = document.createElement('button');
        button.innerHTML = 'Create Grid (Run First!)';
        button.id = 'gridBtn';
        button.onclick = () => {
            if (grid) {
                document.body.removeChild(grid);
            }
            var grid = new Grid(Global.rows, Global.cols, Global.grid, Global.canvasHeight, Global.canvasWidth);

            let span = document.getElementById('span');
            span.style.color = 'rgb(' + ~~(Math.random() * 255) + ',' + ~~(Math.random() * 255) + ',255)';
            span.innerHTML = 'Grid has been Generated TODO: Show grid';

        };
        document.body.appendChild(button);

        let span = document.createElement('span');
        span.id = 'span';
        document.body.appendChild(span);
        document.body.appendChild(document.createElement('br'));

    }
}

class Grid {
    constructor(rows, cols, grid, canvasHeight, canvasWidth) {
        this.createCanvas(canvasHeight, canvasWidth);
        this.create2DArray(rows, cols, grid);
    }
    createCanvas(height, width) {
        //Create HTML5 Canvas
        let canvas = document.createElement('canvas');
        canvas.id = 'grid';
        canvas.width = height;
        canvas.height = width;
        document.body.appendChild(canvas);
        console.log('Grid Created');
    }
    create2DArray(rows, cols, grid) {
        //Create 2D Array
        for (let i = 0; i < cols; i++) {
            grid[i] = new Array(rows);
        }
        this.createCells(rows, cols, grid);
    }
    createCells(rows, cols, grid) {
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                grid[i][j] = new Cell(i, j, false);
            }
        }
        for (let i = 0; i < cols; i++) {
            for (let j = 0; j < rows; j++) {
                grid[i][j].addNeighbors(Global.grid);
            }
        }
    }
}

class Cell {
    constructor(i, j, diagonals) {
        this.x = i;
        this.y = j;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.neighbors = [];
        this.previous = undefined;
        this.wall = false;
        this.diagonals = diagonals;

        if (Math.random() < 0.2) {
            this.wall = true;
        }
    }
    show(col) {
        let canvas = document.getElementById('grid');
        let ctx = canvas.getContext('2d');
        let w = canvas.width / Global.cols;
        let h = canvas.height / Global.rows;
        ctx.fillStyle = 'rgb(' + col[0] + ',' + col[1] + ',' + col[2] + ')';

        if (this.wall) ctx.fillStyle = 'rgb(0,0,0)';

        //this.drawGrid(); DON't RUN THIS, WILL CRASH V8 ENGINE (I THINK?) + It's not necessary
        ctx.fillRect(this.x * w, this.y * h, w - 1, h - 1);
    }
    drawGrid() {
        let canvas = document.getElementById('grid');
        let ctx = canvas.getContext('2d');
        let w = canvas.width;
        let h = canvas.height;
        let step = this.x * (w / Global.cols);


        ctx.beginPath();
        for (let i = 0; i < w; i += step) {
            ctx.moveTo(i, 0);
            ctx.lineTo(i, h);
        }
        ctx.strokeStyle = '#FFF';
        ctx.lineWidth = 1;
        ctx.stroke();

        ctx.beginPath();
        for (let i = 0; i < h; i += step) {
            ctx.moveTo(0, i);
            ctx.lineTo(w, i);
        }
        ctx.strokeStyle = '#000';
        ctx.lineWidth = 1;
        ctx.stroke();
    }
    addNeighbors(grid) {
        let x = this.x;
        let y = this.y;
        if (this.x < Global.cols - 1) this.neighbors.push(grid[x + 1][y]);
        if (this.y < Global.rows - 1) this.neighbors.push(grid[x][y + 1]);
        if (this.x > 0) this.neighbors.push(grid[x - 1][y]);
        if (this.y > 0) this.neighbors.push(grid[x][y - 1]);

        if (this.diagonals) {
            if (this.x > 0 && this.y > 0) this.neighbors.push(grid[this.x - 1][this.y - 1]);
            if (this.x < Global.cols - 1 && this.y > 0) this.neighbors.push(grid[this.x + 1][this.y - 1]);
            if (this.x > 0 && this.y < Global.rows - 1) this.neighbors.push(grid[this.x - 1][this.y + 1]);
            if (this.x < Global.cols - 1 && this.y < Global.rows - 1) this.neighbors.push(grid[this.x + 1][this.y + 1]);
        }


    }
}

class Astar {
    constructor(canvas, rows, cols, grid, interval) {
        this.canvas = canvas;
        this.ctx = canvas.getContext('2d');
        this.openSet = [];
        this.closedSet = [];
        this.grid = grid;
        //this.start = grid[~~(Math.random() * (cols - 1))][~~(Math.random() *(rows - 1))];
        this.start = grid[0][0];
        //this.end = grid[~~(Math.random() * (cols - 1))][~~(Math.random() *(rows - 1))];
        this.end = grid[cols - 1][rows - 1];
        this.rows = rows;
        this.cols = cols;
        this.path = [];
        this.interval = interval;

        this.start.wall = false;
        this.end.wall = false;

        this.openSet.push(this.start);

    }
    run() {
        var p1 = performance.now();
        //Background to Black
        this.ctx.fillStyle = '#000';
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

        if (this.openSet.length > 0) {
            //Keep on Going
            let current = 0;
            for (let i = 0; i < this.openSet.length; i++) {
                if (this.openSet[i].f < this.openSet[current].f) {
                    current = i;
                }
            }

            let currentCell = this.openSet[current];

            if (currentCell === this.end) {
                console.log('A* Function Finished');
                window.clearInterval(this.interval);
            }

            this.closedSet.push(currentCell);
            this.remove(this.openSet, currentCell);

            let neighbors = currentCell.neighbors;
            let betterG = false;
            for (let i = 0; i < neighbors.length; i++) {
                let neighbor = neighbors[i];

                if (!this.closedSet.includes(neighbor) && !neighbor.wall) {
                    let tempG = neighbor.g; //+ Distance between the two nodes

                    if (this.openSet.includes(neighbor)) {
                        if (tempG < neighbor.g) {
                            neighbor.g = tempG;
                            betterG = true;
                        }
                    } else {
                        neighbor.g = tempG;
                        betterG = true;
                        this.openSet.push(neighbor);
                    }

                    if (betterG) {
                        neighbor.h = this.heuristic(neighbor, this.end);
                        neighbor.f = neighbor.g + neighbor.h;
                        neighbor.previous = currentCell;
                    }
                }
            }
            this.getPath(currentCell);
        } else {
            // no solution;
            window.clearInterval(this.interval);
            window.alert('No Possible Solution Found!');

        }
        this.getColors();
        var p2 = performance.now();
        console.log('Time in Seconds: ' + (p2-p1)/1000);
    }
    heuristic(pos, end) {
        //Euclidean Distance
        let a = pos.x - end.x;
        let b = pos.y - end.y;

        return Math.sqrt(a * a + b * b); //Pythagorean Theorem? Nice.
    }
    remove(arr, removee) {
        for (let i = arr.length - 1; i >= 0; i--) {
            if (arr[i] === removee) {
                arr.splice(i, 1);
            }
        }
    }
    getPath(currentCell) {
        this.path = [];
        let temp = currentCell;
        this.path.push(temp);
        while (temp.previous) {
            this.path.push(temp.previous);
            temp = temp.previous;
        }
    }
    getColors() {
        for (let i = 0; i < this.cols; i++) {
            for (let j = 0; j < this.rows; j++) {
                this.grid[i][j].show([255, 255, 255]);
            }
        }
        for (let i = 0; i < this.closedSet.length; i++) {
            this.closedSet[i].show([255, 0, 0]);
        }

        for (let i = 0; i < this.openSet.length; i++) {
            this.openSet[i].show([0, 255, 0]);
        }
        console.log(this.path.length);
        for (let i = 0; i < this.path.length; i++) {
            this.path[i].show([0, 0, 255]);
        }
    }
    randomColors() {
        for (let i = 0; i < this.cols; i++) {
            for (let j = 0; j < this.rows; j++) {
                this.grid[i][j].show([~~(Math.random() * 255), ~~(Math.random() * 255), ~~(Math.random() * 255)]);
            }
        }
    }
}

new Buttons();