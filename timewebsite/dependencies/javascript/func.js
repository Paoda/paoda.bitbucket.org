var timeString;
var toggle = true;
var button;

function getDate() {
    //Declaring Variables and Getting date(); set up
	var nowDate = new Date();
    var nowHour = nowDate.getHours();
    var nowMinute = nowDate.getMinutes();
    var nowSecond = nowDate.getSeconds();
    //Making Minute and Seconds look normal

	if (toggle) {

		nowMinute = nowMinute < 10 ? ("0"+ nowMinute) : nowMinute;
		nowSecond = nowSecond < 10 ? ("0"+ nowSecond) : nowSecond;

    	//Converting Time into 12 Hour Time. 
    	if (nowHour <= 11 && nowHour != 0) {
        	timeString = nowHour + ":" + nowMinute + ":" + nowSecond + " AM";
    	}
    	else if (nowHour == 0) {
        	nowHour = 12;
        	timeString = nowHour + ":" + nowMinute + ":" + nowSecond + " AM";
    	}
    	else if (nowHour == 12){
       		timeString = nowHour + ":" + nowMinute + ":" + nowSecond + " PM";
    	}
		else{
        	nowHour = nowHour - 12;
        	timeString = nowHour + ":" + nowMinute + ":" + nowSecond + " PM";
    	}
    	//Overwriting <a> tag with time
    	document.getElementById("time").innerHTML = timeString;
	} else {
		//Variables
		var nowDate = new Date();
    	var nowHour = nowDate.getHours();
    	var nowMinute = nowDate.getMinutes();
    	var nowSecond = nowDate.getSeconds();
		
		nowMinute = nowMinute < 10 ? ("0"+ nowMinute) : nowMinute;
		nowSecond = nowSecond < 10 ? ("0"+ nowSecond) : nowSecond;
	
		timeString = nowHour + ":" + nowMinute + ":" + nowSecond;
		document.getElementById("time").innerHTML = timeString;
	}
}
function change() {
	toggle = toggle ? false : true;
	button = toggle ? "24h Time" : "12h Time";
	document.getElementById("timeswitch").innerHTML = button;
}
